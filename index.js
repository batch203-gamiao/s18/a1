console.log("Hello World");

function addTwoNumbers(firstNumber,secondNumber){
	console.log("Displayed sum of " + firstNumber + " and " + secondNumber);
	console.log(firstNumber+secondNumber);
}

addTwoNumbers(5,15);

function subTwoNumbers(firstNumber,secondNumber){
	console.log("Displayed difference of " + firstNumber + " and " + secondNumber);
	console.log(firstNumber-secondNumber);
}

subTwoNumbers(20,5);

function mulTwoNumbers(firstNumber,secondNumber){
	console.log("The product of " + firstNumber + " and " + secondNumber + ":");
	return firstNumber*secondNumber;
}

let product = mulTwoNumbers(50,10);
console.log(product);

function divTwoNumbers(firstNumber,secondNumber){
	console.log("The quotient of " + firstNumber + " and " + secondNumber + ":");
	return firstNumber/secondNumber;
}

let quotient = divTwoNumbers(50,10);
console.log(quotient);

function calculateCircleArea(num){
	console.log("The result of getting the area of a circle with " + num + " radius:");

	return (num * num) * Math.PI ;
}

let circleArea = calculateCircleArea(15);
console.log(Number(circleArea.toFixed(2)));

function aveFourNumbers(firstNumber,secondNumber,thirdNumber,fourthNumber){
	
	console.log("The average of " + firstNumber + "," + secondNumber + "," +thirdNumber + "," +fourthNumber + ":");
	return (firstNumber + secondNumber + thirdNumber + fourthNumber) / aveFourNumbers.length;
}

let averageVar = aveFourNumbers(20,40,60,80);
console.log(averageVar);

function getPercentage(firstNumber,secondNumber){
	console.log("Is " + firstNumber + "/" + secondNumber + " a passing score?");
	let result = (firstNumber / secondNumber) * 100;
	let isPassed = result >= 75;

	return isPassed;
}

let isPassingScore = getPercentage(38,50);
console.log(isPassingScore);